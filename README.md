Implementing an RLE-only DEFLATE encoder
========================================
`deflate.c` contains a conventional unix command line program that accepts
commands for compressing files to the gzip format, and for generating a
sample png file.

The core of these file formats is the DEFLATE algorithm and compression
format. DEFLATE is based on an LZ dictionary coder whose output is then
fed to a Huffman entropy coder. LZ coder iterates over the input byte array
and finds backward references to strings that match the string that begins
at the current position. These matches are coded as `(distance,length)`
pairs where `distance` is the backward distance from the current position,
and `length` is the length of the match. Then a good combination of these
backward references and literal bytes is selected and fed to the Huffman
coder that further compresses the stream by translating frequently
occurring symbols to shorter bit strings.

RLE-based algorithm considers only those backward references that are of the
form `(1,length)`. Note that these references extend past the _current
position_, and that matching and expanding them is natural to any LZ-based
DEFLATE algorithm without extra work.

The gain of RLE-only approach is time efficiency and low memory usage. In
fact RLE combined with static Huffman codes is probably the most minimal
approach to DEFLATE that achieves compression. Time complexity of both the
RLE encoder and decoder is linear, they iterate once over the input array.
Memory usage is a few state variables.

Obvious downside is low compression ratio. For instance `deflate.c`
compresses from 11874 to 11138 bytes, mostly because of indentation (spaces).
Interestingly png files sometimes compress well with rle, for instance
screenshots and other generated images that contain large areas of flat
color surface.

Interface and description of basic RLE algorithm
================================================
```
struct DeflateRLEContext {
  uint64  bit_buffer;
  uint32  bit_buffer_position;
  uint32  run_byte;
  uint32  run_length;
  uint8  *output;
  uint32  output_count;
};

void generate_lookup_tables();

void initialize_deflate_rle_context(struct DeflateRLEContext *context, uint64 block_header, uint8 first_input_byte, uint8 *output);
void finalize_deflate_rle_context(struct DeflateRLEContext *context);
void encode_deflate_rle(struct DeflateRLEContext *context, uint8 *input, uint32 input_length);

void encode_gzip(uint8 *input, uint32 input_length, uint8 *output, uint32 *encoded_byte_count);
void encode_png(uint8 *input, uint32 width, uint32 height, uint8 *output, uint32 *encoded_byte_count);
```

`encode_gzip` and `encode_png` are the high-level functions that generate
gzip and png files. `input` to `encode_png` must be 8-bit RGB data as
described in [RFC 2083](https://tools.ietf.org/html/rfc2083). Output buffers
must be allocated large enough by the user. Note that if the input data
doesn't contain enough byte runs, it is possible that the size of the
compressed output is greater than the input. Maximum expansion is about one
bit per input byte.

`generate_lookup_tables` must be called before calling the encoding
functions. Lookup tables contain the static Huffman codes that are prepared
for easy outputing to the bitstream. Each Huffman code (including _extra
bits_) fits into a 16-bit word. For literal bytes there are 256 lookup table
entries, and for _lengths_ there are two times 258 entries, where the other
table is for the number of bits. For extreme memory-constrained systems it
is possible to do without lookup tables by sacrificing time efficiency.

`struct DeflateRLEContext` holds the state of the encoding process. It
enables `encode_deflate_rle` to be called multiple times with different
input sources to produce a single output stream. Specifically it is needed
for multiplexing the png _filter type_ bytes (these are always 0, as no
filtering is used) and the raw input image data together into a single
DEFLATE stream without doing expensive buffering. Before the first call to
`encode_deflate_rle` the state structure must be initialized with
`initialize_deflate_rle_context`, and after the last call to
`encode_deflate_rle` the RLE encoder must be flushed to output the last
unencoded run and to end the DEFLATE stream by calling
`finalize_deflate_rle_context`.

`encode_deflate_rle` is the inner loop, it works by iterating over the input
and for each byte considering whether it continues or ends the current
_run_. Shortest match that DEFLATE can code is 3 bytes (longest is 258
bytes), so we need a 4 byte run before we can code it as a 1 byte _literal_
followed by a 3 byte _match_. Shorter than 4 byte runs are coded as a
sequence of 1 to 3 literals. Coding a bitstream to a sequence of bytes is
costly, therefore we use one `uint64` register as a buffer (`bit_buffer`) to
hold multiple writes before they are outputed to the byte buffer. Shorter
than 4 byte runs are encoded with maximum of `3 * 9 = 27` bits, equal to
4 byte or greater runs are encoded with maximum of `9 + (8 + 5 + 5) = 27`
bits, therefore `uint64` is always enough to hold one complete run (`uint32`
is not). This design allows to do the costly write to the output byte array
only once per run, without having to do explicit boundary tests.

### Possible optimizations

The implementation is optimized for low redundancy and improved
readability. It would be possible to produce time optimized inner loops by
inlining the code that is currently abstracted to functions. Possibly by
giving `inline` directives to gcc. `struct DeflateRLEContext` wouldn't be
needed by inlined versions.

Code for calculating crc-32 and adler-32 checksums is copied from the
related rfcs. To reduce overhead these calculations can be disabled when
they are either unnecessary or uncritical.

### Dependencies

The encoder assumes little-endian machine by using pointer dereferences to
store/fetch words into memory. At the same time it is doing unaligned memory
accesses. It is also assumed that `long` is a 64-bit integer.

The driver code in `main` depends on the x86-64 linux syscall interface.

Usage
=====
```
$ gcc -w -nostdlib -z execstack -o deflate deflate.c
$ ./deflate gzip-compress deflate.c test-output.gz
$ ./deflate png-compress xor-pattern-output.png
```
