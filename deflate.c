typedef unsigned long  uint64;
typedef unsigned int   uint32;
typedef unsigned short uint16;
typedef unsigned char  uint8;
typedef long           int64;
typedef int            int32;
typedef short          int16;
typedef char           int8;

enum {
  MAP_PRIVATE       = 0x02,
  MAP_ANONYMOUS     = 0x20,
  MAP_UNINITIALIZED = 0x4000000
};

enum {
  PROT_READ  = 1,
  PROT_WRITE = 2,
};

enum {
  O_RDONLY = 00000,
  O_RDWR   = 00002,
  O_CREAT  = 00100,
  O_TRUNC  = 01000
};

struct stat {
  uint64 st_dev;
  uint64 st_ino;
  uint64 st_nlink;
  uint32 st_mode;
  uint32 st_uid;
  uint32 st_gid;
  uint32 __pad0;
  uint64 st_rdev;
  uint64 st_size;
  uint64 st_atime;
  uint64 st_atime_nsec;
  uint64 st_mtime;
  uint64 st_mtime_nsec;
  uint64 st_ctime;
  uint64 st_ctime_nsec;
  uint64 st_blksize;
  int64  st_blocks;
  uint64 __unused[3];
};

uint64 syscall_instructions[] = { 0x8948F78948F88948, 0x4DC2894DCA8948D6, 0x0F08244C8B4CC889, 0xC305 };
uint64 (*syscall)(uint64, uint64, uint64, uint64, uint64, uint64, uint64) = syscall_instructions;

int32  write(uint32 fd, void *buffer, uint32 count)                                         { return syscall(1, fd, buffer, count, 0, 0, 0); }
int32  open(uint8 *filename, uint32 flags, uint32 mode)                                     { return syscall(2, filename, flags, mode, 0, 0, 0); }
int32  fstat(uint32 fd, struct stat *statbuf)                                               { return syscall(5, fd, statbuf, 0, 0, 0, 0); }
void  *mmap(void *addr, uint32 length, uint32 prot, uint32 flags, uint32 fd, uint32 offset) { return syscall(9, addr, length, prot, flags, fd, offset); }

void *allocate_pages(uint32 byte_count) {
  return mmap(0, byte_count, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_UNINITIALIZED, 0, 0);
}

uint32 c_strings_equal(uint8 *a, uint8 *b) {
  while (*a && *b && (*(a++) == *(b++)));
  return (!(*a)) && (!(*b));
}

uint32 crc_table[256];

void make_crc_table() {
  uint32 c;
  for (int n = 0; n < 256; n++) {
    c = (uint32) n;
    for (uint32 k = 0; k < 8; k++) {
      if (c & 1) {
        c = 0xEDB88320 ^ (c >> 1);
      } else {
        c = c >> 1;
      }
    }
    crc_table[n] = c;
  }
}

uint32 crc(uint8 *buf, uint32 len) {
  uint32 c = 0xFFFFFFFF;
  for (uint32 n = 0; n < len; n++) {
    c = crc_table[(c ^ buf[n]) & 0xFF] ^ (c >> 8);
  }
  return c ^ 0xFFFFFFFF;
}

uint32 update_adler(uint32 adler, uint8 *buf, uint32 len) {
  uint32 s1 = adler         & 0xFFFF;
  uint32 s2 = (adler >> 16) & 0xFFFF;
  for (uint32 n = 0; n < len; n++) {
    s1 = (s1 + buf[n]) % 65521;
    s2 = (s2 + s1)     % 65521;
  }
  return (s2 << 16) | s1;
}

uint32 literal_lookup[256];
uint32 length_lookup[259];
uint32 length_bits_lookup[259];

uint32 reverse(uint32 value, uint32 bit_count) {
  uint32 reversed = 0;
  for (uint32 i = 0; i < bit_count; i++) {
    reversed |= (value & (1 << i)) ? (1 << (bit_count - 1 - i)) : 0;
  }
  return reversed;
}

void generate_literal_lookup_table(uint32 code, uint32 first_literal, uint32 last_literal, uint32 bits) {
  for (uint32 i = first_literal; i <= last_literal; i++) {
    literal_lookup[i] = reverse(code++, bits);
  }
}

void generate_length_lookup_tables(uint32 code, uint32 first_length, uint32 last_length, uint32 huffman_bits, uint32 extra_bits) {
  for (uint32 i = first_length; i <= last_length; i++) {
    uint32 offset         = i - first_length;
    length_lookup[i]      = reverse(code + (offset >> extra_bits), huffman_bits) | ((offset & ((1 << extra_bits) - 1)) << huffman_bits);
    length_bits_lookup[i] = huffman_bits + extra_bits;
  }
}

void generate_lookup_tables() {
  {
    generate_literal_lookup_table(0b00110000,    0, 143, 8);
    generate_literal_lookup_table(0b110010000, 144, 255, 9);
  }
  {
    uint32 code;
    generate_length_lookup_tables(code  = 0b0000001,    3,  10, 7, 0);
    generate_length_lookup_tables(code += 8,           11,  18, 7, 1);
    generate_length_lookup_tables(code += 4,           19,  34, 7, 2);
    generate_length_lookup_tables(code += 4,           35,  66, 7, 3);
    generate_length_lookup_tables(code += 4,           67, 114, 7, 4);
    generate_length_lookup_tables(code  = 0b11000000, 115, 130, 8, 4);
    generate_length_lookup_tables(code += 1,          131, 257, 8, 5);
    {
      uint32 offset           = 258 - 131;
      length_lookup[258]      = reverse(code + (offset >> 5), 8);
      length_bits_lookup[258] = 8;
    }
  }
}

struct DeflateRLEContext {
  uint64  bit_buffer;
  uint32  bit_buffer_position;
  uint32  run_byte;
  uint32  run_length;
  uint8  *output;
  uint32  output_count;
};

void *copy_memory(uint8 *destination, uint8 *source, uint32 byte_count) {
  while (byte_count--) {
    *(destination++) = *(source++);
  }
  return destination;
}

void *copy_memory_reverse(uint8 *destination, uint8 *source, uint32 byte_count) {
  while (byte_count--) {
    *(destination++) = *(source + byte_count);
  }
  return destination;
}

void *fill_memory(uint8 *destination, uint8 byte, uint32 count) {
  while (count--) {
    *(destination++) = byte;
  }
  return destination;
}

void encode_deflate_rle_bits(struct DeflateRLEContext *context, uint64 value, uint32 bit_count) {
  context->bit_buffer          |= value << context->bit_buffer_position;
  context->bit_buffer_position += bit_count;
}

void encode_deflate_rle_literal(struct DeflateRLEContext *context) {
  encode_deflate_rle_bits(context, literal_lookup[context->run_byte], (context->run_byte <= 143) ? 8 : 9);
}

void encode_deflate_rle_flush(struct DeflateRLEContext *context) {
  uint64 byte_count              = context->bit_buffer_position / 8;
  *((uint64 *) context->output)  = context->bit_buffer;
  context->output               += byte_count;
  context->output_count         += byte_count;
  context->bit_buffer          >>= byte_count * 8;
  context->bit_buffer_position  -= byte_count * 8;
}

void encode_deflate_rle_run(struct DeflateRLEContext *context) {
  if (context->run_length >= 4) {
    encode_deflate_rle_literal(context);
    encode_deflate_rle_bits(context, length_lookup[context->run_length - 1], length_bits_lookup[context->run_length - 1] + 5);
  } else {
    switch (context->run_length) {
      case 3: encode_deflate_rle_literal(context);
      case 2: encode_deflate_rle_literal(context);
      case 1: encode_deflate_rle_literal(context);
    }
  }
  encode_deflate_rle_flush(context);
}

void initialize_deflate_rle_context(struct DeflateRLEContext *context, uint64 block_header, uint8 first_input_byte, uint8 *output) {
  context->bit_buffer          = block_header;
  context->bit_buffer_position = 3;
  context->run_byte            = first_input_byte;
  context->run_length          = 1;
  context->output              = output;
  context->output_count        = 0;
}

void finalize_deflate_rle_context(struct DeflateRLEContext *context) {
  encode_deflate_rle_run(context);
  {
    context->bit_buffer_position += 7 + 7;
    encode_deflate_rle_flush(context);
  }
}

void encode_deflate_rle(struct DeflateRLEContext *context, uint8 *input, uint32 input_length) {
  while (input_length--) {
    if ((input[0] == context->run_byte) && (context->run_length < 258)) {
      context->run_length++;
    } else {
      encode_deflate_rle_run(context);
      context->run_byte   = input[0];
      context->run_length = 1;
    }
    input++;
  }
}

void encode_gzip(uint8 *input, uint32 input_length, uint8 *output, uint32 *encoded_byte_count) {
  copy_memory(output, (uint8 []) { 0x1F, 0x8B, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF }, 10);
  struct DeflateRLEContext context;
  initialize_deflate_rle_context(&context, 0x03, input[0], output + 10);
  encode_deflate_rle(&context, input + 1, input_length - 1);
  finalize_deflate_rle_context(&context);
  *((uint32 *) (output + context.output_count + 10)) = crc(input, input_length);
  *((uint32 *) (output + context.output_count + 14)) = input_length;
  *encoded_byte_count                                = 10 + context.output_count + 8;
}

void encode_png(uint8 *input, uint32 width, uint32 height, uint8 *output, uint32 *encoded_byte_count) {
  uint32 checksum;
  uint8 *ihdr = copy_memory(output, (uint8 []) { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A }, 8);
  uint32 chunk_length = 13;
  copy_memory_reverse(ihdr, &chunk_length, 4);
  copy_memory(ihdr + 4, (uint8 []) { 0x49, 0x48, 0x44, 0x52 }, 4);
  copy_memory_reverse(ihdr +  8, &width,  4);
  copy_memory_reverse(ihdr + 12, &height, 4);
  copy_memory(ihdr + 16, (uint8 []) { 0x08, 0x02, 0x00, 0x00, 0x00 }, 5);
  checksum = crc(ihdr + 4, chunk_length + 4);
  copy_memory_reverse(ihdr + 21, &checksum, 4);
  uint8  *idat = ihdr + 25;
  copy_memory(idat + 4, (uint8 []) { 0x49, 0x44, 0x41, 0x54 }, 4);
  {
    struct DeflateRLEContext context;
    checksum = 1;
    {
      copy_memory(idat + 8, (uint8 []) { 0x78, 0x01 }, 2);
    }
    {
      uint8 filter_byte      = 0x00;
      uint32 scanline_length = width * 3;
      initialize_deflate_rle_context(&context, 0x03, filter_byte, idat + 10);
      encode_deflate_rle(&context, input, scanline_length);
      checksum = update_adler(checksum, &filter_byte, 1);
      checksum = update_adler(checksum, input, scanline_length);
      for (uint32 scanline = 1; scanline < height; scanline++) {
        encode_deflate_rle(&context, &filter_byte, 1);
        encode_deflate_rle(&context, input + (scanline * scanline_length), scanline_length);
        checksum = update_adler(checksum, &filter_byte, 1);
        checksum = update_adler(checksum, input + (scanline * scanline_length), scanline_length);
      }
      finalize_deflate_rle_context(&context);
    }
    {
      copy_memory_reverse(context.output, &checksum, 4);
    }
    chunk_length = 6 + context.output_count;
  }
  copy_memory_reverse(idat, &chunk_length, 4);
  checksum = crc(idat + 4, chunk_length + 4);
  uint8 *iend = copy_memory_reverse(idat + 8 + chunk_length, &checksum, 4);
  fill_memory(iend, 0x00, 4);
  copy_memory(iend + 4, (uint8 []) { 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82 }, 8);
  *encoded_byte_count = (iend + 12) - output;
}

void write_file(int8 *path, uint8 *buffer, uint32 buffer_length) {
  write(open(path, O_RDWR | O_CREAT | O_TRUNC, 0600), buffer, buffer_length);
}

int32 main(int32 argc, int8 *argv[]) {
  uint8  *output;
  uint32  output_length;
  uint8  *input;
  uint32  input_length;
  {
    generate_lookup_tables();
    make_crc_table();
  }
  if (c_strings_equal("gzip-compress", argv[1])) {
    {
      {
        int32 fd = open(argv[2], O_RDONLY, 0);
        struct stat statbuf;
        fstat(fd, &statbuf);
        input_length = statbuf.st_size;
        input        = mmap(0, input_length, PROT_READ, MAP_PRIVATE, fd, 0);
      }
      {
        output = allocate_pages(input_length + (input_length / 8));
      }
    }
    {
      encode_gzip(input, input_length, output, &output_length);
      write_file(argv[3], output, output_length);
    }
  } else if (c_strings_equal("png-compress", argv[1])) {
    uint32 width  = 640;
    uint32 height = 480;
    {
      input_length = width * height * 3;
      input        = allocate_pages(input_length);
      output       = allocate_pages(input_length + (input_length / 8));
    }
    {
      for (uint32 y = 0; y < height; y++) {
        if ((y & 15) > 3) {
          for (uint32 x = 0; x < width; x++) {
            input[(y * width + x) * 3]     = (x ^ y);
            input[(y * width + x) * 3 + 1] = (x ^ y) << 1;
            input[(y * width + x) * 3 + 2] = (x ^ y);
          }
        } else {
          fill_memory(input + (y * width * 3), 0x00, width * 3);
        }
      }
    }
    {
      encode_png(input, width, height, output, &output_length);
      write_file(argv[2], output, output_length);
    }
  }
  return 0;
}

uint64 _start[] = { 0x24748D48243C8B48, 0xB8489048E5894808, &main, 0x003CB8C78948D0FF, 0x050F0000 };
